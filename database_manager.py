import mysql.connector

class DBManager:
    def __init__(self):    
        self.db = mysql.connector.connect(
            host = 'sql7.freemysqlhosting.net',
            username = 'sql7366741',
            password = 'uQKSiw71Xp',
            database = 'sql7366741'
        )
    
    def fetch_post_module(self, post_number):
        query = f'''SELECT Module FROM Posts WHERE ID = {post_number}'''
        cursor = self.db.cursor()
        try:
            cursor.execute(query)
            return cursor.fetchall()[0][0]
        except Exception as e:
            print(f'{e}')
            return None
    
    def fetch_all_data(self):
        query = '''SELECT * FROM Posts ORDER BY ID'''
        cursor = self.db.cursor()
        try:
            cursor.execute(query)
            return cursor.fetchall()
        except Exception as e:
            print(f'{e}')
            return None
    
    def update_post_module(self, post_number, new_module):
        query = f''' UPDATE Posts SET Module = {new_module} WHERE ID = {post_number} '''
        cursor = self.db.cursor()
        try:
            cursor.execute(query)
            self.db.commit()
            return True
        except Exception as e:
            print(f'{e}')
            return False
    
    def insert_data(self, post_number, new_module):
        query = f'''INSERT INTO Posts VALUES({post_number}, {new_module}) '''
        cursor = self.db.cursor()
        try:
            cursor.execute(query)
            self.db.commit()
            return True
        except Exception as e:
            print(f'{e}')
            return False