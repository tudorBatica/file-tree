# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'db_page.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!

from database_manager import DBManager
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox


class Ui_DBWindow(object):
    def setupUi(self, DBWindow):
        DBWindow.setObjectName("DBWindow")
        DBWindow.resize(900, 861)
        self.centralwidget = QtWidgets.QWidget(DBWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.bottom_frame = QtWidgets.QFrame(self.centralwidget)
        self.bottom_frame.setGeometry(QtCore.QRect(0, 0, 901, 261))
        self.bottom_frame.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(251, 251, 251, 255), stop:1 rgba(244, 244, 244, 255));")
        self.bottom_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.bottom_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.bottom_frame.setObjectName("bottom_frame")
        self.label = QtWidgets.QLabel(self.bottom_frame)
        self.label.setGeometry(QtCore.QRect(0, 10, 901, 20))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.post_number_text_edit = QtWidgets.QPlainTextEdit(self.bottom_frame)
        self.post_number_text_edit.setGeometry(QtCore.QRect(200, 70, 281, 31))
        self.post_number_text_edit.setStyleSheet("background-color: #ffffff;")
        self.post_number_text_edit.setPlainText("")
        self.post_number_text_edit.setObjectName("post_number_text_edit")
        self.post_number_label = QtWidgets.QLabel(self.bottom_frame)
        self.post_number_label.setGeometry(QtCore.QRect(10, 70, 171, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.post_number_label.setFont(font)
        self.post_number_label.setStyleSheet("background-color: transparent;\n"
"")
        self.post_number_label.setObjectName("post_number_label")
        self.module_label = QtWidgets.QLabel(self.bottom_frame)
        self.module_label.setGeometry(QtCore.QRect(10, 150, 151, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.module_label.setFont(font)
        self.module_label.setStyleSheet("background-color: transparent;\n"
"")
        self.module_label.setObjectName("module_label")
        self.module1_radio_button = QtWidgets.QRadioButton(self.bottom_frame)
        self.module1_radio_button.setGeometry(QtCore.QRect(200, 150, 95, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.module1_radio_button.setFont(font)
        self.module1_radio_button.setStyleSheet("color: #6b7179;\n"
"font-weight: bold;")
        self.module1_radio_button.setObjectName("module1_radio_button")
        self.module2_radio_button = QtWidgets.QRadioButton(self.bottom_frame)
        self.module2_radio_button.setGeometry(QtCore.QRect(370, 150, 95, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.module2_radio_button.setFont(font)
        self.module2_radio_button.setStyleSheet("color: #6b7179;\n"
"font-weight: bold;")
        self.module2_radio_button.setObjectName("module2_radio_button")
        self.module12_radio_button = QtWidgets.QRadioButton(self.bottom_frame)
        self.module12_radio_button.setGeometry(QtCore.QRect(540, 150, 111, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.module12_radio_button.setFont(font)
        self.module12_radio_button.setStyleSheet("color: #6b7179;\n"
"font-weight: bold;")
        self.module12_radio_button.setObjectName("module12_radio_button")
        self.modify_db_button = QtWidgets.QPushButton(self.bottom_frame)
        self.modify_db_button.setGeometry(QtCore.QRect(10, 200, 351, 41))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setPointSize(-1)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.modify_db_button.setFont(font)
        self.modify_db_button.setStyleSheet("color: white;\n"
"font: 63 8pt \"Bahnschrift SemiBold\";\n"
"background-color: #3597f9;\n"
"font-weight: bold;\n"
"font-size: 16px;\n"
"border-radius: 5px;\n"
"\n"
"")
        self.modify_db_button.setObjectName("modify_db_button")
        self.frame_3 = QtWidgets.QFrame(self.centralwidget)
        self.frame_3.setGeometry(QtCore.QRect(0, 290, 901, 541))
        self.frame_3.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(251, 250, 245, 255), stop:1 rgba(243, 242, 247, 255));\n"
"\n"
"border-radius: 5px;")
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.selected_folders_scroll_area = QtWidgets.QScrollArea(self.frame_3)
        self.selected_folders_scroll_area.setGeometry(QtCore.QRect(0, 60, 901, 471))
        self.selected_folders_scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.selected_folders_scroll_area.setWidgetResizable(True)
        self.selected_folders_scroll_area.setObjectName("selected_folders_scroll_area")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 880, 471))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.db_entries_label = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.db_entries_label.setFont(font)
        self.db_entries_label.setObjectName("db_entries_label")
        self.verticalLayout.addWidget(self.db_entries_label)
        self.selected_folders_scroll_area.setWidget(self.scrollAreaWidgetContents)
        self.label_2 = QtWidgets.QLabel(self.frame_3)
        self.label_2.setGeometry(QtCore.QRect(0, 20, 901, 20))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        DBWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(DBWindow)
        self.statusbar.setObjectName("statusbar")
        DBWindow.setStatusBar(self.statusbar)

        self.retranslateUi(DBWindow)
        QtCore.QMetaObject.connectSlotsByName(DBWindow)

        self.modify_db_button.clicked.connect(self.modify_database)
        self.show_database_records()


    def retranslateUi(self, DBWindow):
        _translate = QtCore.QCoreApplication.translate
        DBWindow.setWindowTitle(_translate("DBWindow", "MainWindow"))
        self.label.setText(_translate("DBWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">MODIFICARE BAZA DE DATE</span></p></body></html>"))
        self.post_number_label.setText(_translate("DBWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#787d86;\">Numarul Postului:</span></p></body></html>"))
        self.module_label.setText(_translate("DBWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#787d86;\">Modulul:</span></p></body></html>"))
        self.module1_radio_button.setText(_translate("DBWindow", "Modul 1"))
        self.module2_radio_button.setText(_translate("DBWindow", "Modul 2"))
        self.module12_radio_button.setText(_translate("DBWindow", "Modul 12"))
        self.modify_db_button.setText(_translate("DBWindow", "MODIFICA"))
        self.db_entries_label.setText(_translate("DBWindow", "<html><head/><body><p align=\"center\"/></body></html>"))
        self.label_2.setText(_translate("DBWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">INREGISTRARI IN BAZA DE DATE</span></p></body></html>"))

    def modify_database(self):
        input_error = self.user_input_errors()
        if input_error:
            QMessageBox.warning(None, "Completare Date", input_error, QMessageBox.Ok)
            return
        
        if self.module1_radio_button.isChecked():
            module = 1
        elif self.module2_radio_button.isChecked():
            module = 2
        else:
            module = 12
        
        self.add_record_to_database(self.post_number_text_edit.toPlainText().strip(), module)


    def user_input_errors(self):
        if not self.post_number_text_edit.toPlainText().strip().isdigit():
            return "Introduceti un numar valid de post"
        
        if not self.module1_radio_button.isChecked() and not self.module2_radio_button.isChecked() and not self.module12_radio_button.isChecked():
            return "Selectati un modul"
        
        return ""

    def add_record_to_database(self, post, module):
        db = DBManager()
        if not db.insert_data(post, module):
            db.update_post_module(post, module)
        self.show_database_records()
    
    def show_database_records(self):
        db = DBManager()
        records = db.fetch_all_data()
        records_string = ""
        for record in records:
            records_string += f'Post {record[0]}  -  Modul {record[1]}\n'
        self.db_entries_label.setText(records_string)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    DBWindow = QtWidgets.QMainWindow()
    ui = Ui_DBWindow()
    ui.setupUi(DBWindow)
    DBWindow.show()
    sys.exit(app.exec_())
