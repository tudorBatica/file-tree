import restructurer
import sys
import webbrowser
from db_page import Ui_DBWindow
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from report_page import Ui_ReportWindow

class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1274, 867)
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        MainWindow.setFont(font)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(240, 241, 243, 255), stop:1 rgba(230, 233, 240, 255));")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(460, 80, 791, 311))
        self.frame.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(251, 250, 245, 255), stop:1 rgba(243, 242, 247, 255));\n"
"\n"
"border-radius: 5px;")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.regional_label = QtWidgets.QLabel(self.frame)
        self.regional_label.setGeometry(QtCore.QRect(40, 30, 231, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.regional_label.setFont(font)
        self.regional_label.setStyleSheet("background-color: transparent;\n"
"")
        self.regional_label.setObjectName("regional_label")
        self.regional_text_edit = QtWidgets.QPlainTextEdit(self.frame)
        self.regional_text_edit.setGeometry(QtCore.QRect(410, 30, 281, 31))
        self.regional_text_edit.setStyleSheet("background-color: #ffffff;")
        self.regional_text_edit.setPlainText("")
        self.regional_text_edit.setObjectName("regional_text_edit")
        self.trimester_label = QtWidgets.QLabel(self.frame)
        self.trimester_label.setGeometry(QtCore.QRect(40, 120, 231, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.trimester_label.setFont(font)
        self.trimester_label.setStyleSheet("background-color: transparent;\n"
"")
        self.trimester_label.setObjectName("trimester_label")
        self.trimester_spin_box = QtWidgets.QSpinBox(self.frame)
        self.trimester_spin_box.setGeometry(QtCore.QRect(410, 120, 51, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.trimester_spin_box.setFont(font)
        self.trimester_spin_box.setStyleSheet("font-weight: bold;\n"
"color: #6b7179;")
        self.trimester_spin_box.setMinimum(1)
        self.trimester_spin_box.setMaximum(3)
        self.trimester_spin_box.setObjectName("trimester_spin_box")
        self.deletion_label = QtWidgets.QLabel(self.frame)
        self.deletion_label.setGeometry(QtCore.QRect(40, 220, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.deletion_label.setFont(font)
        self.deletion_label.setStyleSheet("background-color: transparent;\n"
"")
        self.deletion_label.setObjectName("deletion_label")
        self.deletion_yes_radio_button = QtWidgets.QRadioButton(self.frame)
        self.deletion_yes_radio_button.setGeometry(QtCore.QRect(420, 230, 95, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.deletion_yes_radio_button.setFont(font)
        self.deletion_yes_radio_button.setStyleSheet("color: #6b7179;\n"
"font-weight: bold;")
        self.deletion_yes_radio_button.setObjectName("deletion_yes_radio_button")
        self.deletion_no_radio_button = QtWidgets.QRadioButton(self.frame)
        self.deletion_no_radio_button.setGeometry(QtCore.QRect(590, 230, 95, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.deletion_no_radio_button.setFont(font)
        self.deletion_no_radio_button.setStyleSheet("color: #6b7179;\n"
"font-weight: bold;")
        self.deletion_no_radio_button.setObjectName("deletion_no_radio_button")
        self.restructuring_details_frame = QtWidgets.QFrame(self.centralwidget)
        self.restructuring_details_frame.setGeometry(QtCore.QRect(30, 80, 411, 311))
        self.restructuring_details_frame.setStyleSheet("background-color: #fafafa;")
        self.restructuring_details_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.restructuring_details_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.restructuring_details_frame.setObjectName("restructuring_details_frame")
        self.label = QtWidgets.QLabel(self.restructuring_details_frame)
        self.label.setGeometry(QtCore.QRect(10, 30, 391, 20))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_3 = QtWidgets.QLabel(self.restructuring_details_frame)
        self.label_3.setGeometry(QtCore.QRect(160, 80, 91, 91))
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap("info.png"))
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.restructuring_details_frame)
        self.label_4.setGeometry(QtCore.QRect(10, 200, 391, 20))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.restructuring_details_frame)
        self.label_5.setGeometry(QtCore.QRect(10, 240, 391, 20))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.top_frame = QtWidgets.QFrame(self.centralwidget)
        self.top_frame.setGeometry(QtCore.QRect(0, 0, 1281, 61))
        self.top_frame.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(251, 251, 251, 255), stop:1 rgba(244, 244, 244, 255));")
        self.top_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.top_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.top_frame.setObjectName("top_frame")
        
        self.db_button = QtWidgets.QPushButton(self.top_frame)
        self.db_button.setGeometry(QtCore.QRect(30, 0, 175, 61))
        self.db_button.setStyleSheet("border-radius: 5px;\n"
"font: 63 8pt \"Bahnschrift SemiBold\";\n"
"background-color: #3597f9;\n"
"font-weight: bold;\n"
"color: #ffffff;\n"
"")
        self.db_button.setObjectName("db_button")
        
        
        self.help_button = QtWidgets.QPushButton(self.top_frame)
        self.help_button.setGeometry(QtCore.QRect(490, 0, 111, 61))
        self.help_button.setStyleSheet("border-radius: 5px;\n"
"font: 63 8pt \"Bahnschrift SemiBold\";\n"
"background-color: #ffffff;\n"
"font-weight: bold;\n"
"color: #3597f9;\n"
"")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("help_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.help_button.setIcon(icon)
        self.help_button.setIconSize(QtCore.QSize(50, 40))
        self.help_button.setObjectName("help_button")
        self.exit_button = QtWidgets.QPushButton(self.top_frame)
        self.exit_button.setGeometry(QtCore.QRect(610, 0, 111, 61))
        self.exit_button.setStyleSheet("border-radius: 5px;\n"
"font: 63 8pt \"Bahnschrift SemiBold\";\n"
"background-color: #3597f9;\n"
"font-weight: bold;\n"
"color: #ffffff;\n"
"")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("exit_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.exit_button.setIcon(icon1)
        self.exit_button.setIconSize(QtCore.QSize(50, 40))
        self.exit_button.setObjectName("exit_button")
        self.frame_3 = QtWidgets.QFrame(self.centralwidget)
        self.frame_3.setGeometry(QtCore.QRect(460, 410, 791, 341))
        self.frame_3.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(251, 250, 245, 255), stop:1 rgba(243, 242, 247, 255));\n"
"\n"
"border-radius: 5px;")
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.selected_folders_scroll_area = QtWidgets.QScrollArea(self.frame_3)
        self.selected_folders_scroll_area.setGeometry(QtCore.QRect(0, 90, 791, 251))
        self.selected_folders_scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.selected_folders_scroll_area.setWidgetResizable(True)
        self.selected_folders_scroll_area.setObjectName("selected_folders_scroll_area")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 770, 251))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.selected_folders_label = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.selected_folders_label.setFont(font)
        self.selected_folders_label.setObjectName("selected_folders_label")
        self.verticalLayout.addWidget(self.selected_folders_label)
        self.selected_folders_scroll_area.setWidget(self.scrollAreaWidgetContents)
        self.folder_selection_button = QtWidgets.QPushButton(self.frame_3)
        self.folder_selection_button.setGeometry(QtCore.QRect(40, 30, 281, 31))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setPointSize(-1)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.folder_selection_button.setFont(font)
        self.folder_selection_button.setStyleSheet("color: white;\n"
"font: 63 8pt \"Bahnschrift SemiBold\";\n"
"background-color: #3597f9;\n"
"font-weight: bold;\n"
"font-size: 16px;\n"
"\n"
"")
        self.folder_selection_button.setObjectName("folder_selection_button")
        self.folder_selection_button_2 = QtWidgets.QPushButton(self.frame_3)
        self.folder_selection_button_2.setGeometry(QtCore.QRect(450, 30, 281, 31))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setPointSize(-1)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.folder_selection_button_2.setFont(font)
        self.folder_selection_button_2.setStyleSheet("color: white;\n"
"font: 63 8pt \"Bahnschrift SemiBold\";\n"
"background-color: #3597f9;\n"
"font-weight: bold;\n"
"font-size: 16px;\n"
"\n"
"")
        self.folder_selection_button_2.setObjectName("folder_selection_button_2")
        self.frame_5 = QtWidgets.QFrame(self.centralwidget)
        self.frame_5.setGeometry(QtCore.QRect(30, 410, 411, 341))
        self.frame_5.setStyleSheet("background-color: #fafafa;")
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.label_6 = QtWidgets.QLabel(self.frame_5)
        self.label_6.setGeometry(QtCore.QRect(10, 30, 391, 20))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.label_8 = QtWidgets.QLabel(self.frame_5)
        self.label_8.setGeometry(QtCore.QRect(10, 190, 391, 51))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.label_10 = QtWidgets.QLabel(self.frame_5)
        self.label_10.setGeometry(QtCore.QRect(160, 80, 91, 91))
        self.label_10.setText("")
        self.label_10.setPixmap(QtGui.QPixmap("add_folder.png"))
        self.label_10.setScaledContents(True)
        self.label_10.setObjectName("label_10")
        self.label_9 = QtWidgets.QLabel(self.frame_5)
        self.label_9.setGeometry(QtCore.QRect(10, 260, 391, 51))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setObjectName("label_9")
        self.restructure_button = QtWidgets.QPushButton(self.centralwidget)
        self.restructure_button.setGeometry(QtCore.QRect(450, 800, 351, 41))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setPointSize(-1)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.restructure_button.setFont(font)
        self.restructure_button.setStyleSheet("color: white;\n"
"font: 63 8pt \"Bahnschrift SemiBold\";\n"
"background-color: #3597f9;\n"
"font-weight: bold;\n"
"font-size: 16px;\n"
"border-radius: 5px;\n"
"\n"
"")
        self.restructure_button.setObjectName("restructure_button")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.selected_folders = []
        self.folder_selection_button.clicked.connect(self.select_new_folder)
        self.folder_selection_button_2.clicked.connect(self.reset_selected_folders)
        self.restructure_button.clicked.connect(self.restructure)
        self.help_button.clicked.connect(self.open_help)
        self.exit_button.clicked.connect(self.close_window)
        self.db_button.clicked.connect(self.open_database_window)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Restructurare Foldere"))
        self.regional_label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#787d86;\">Numele Regionalei:</span></p></body></html>"))
        self.trimester_label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#6b7179;\">Selectati trimestrul:</span></p></body></html>"))
        self.deletion_label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#6b7179;\">Doriti stergerea folderului initial?</span></p></body></html>"))
        self.deletion_yes_radio_button.setText(_translate("MainWindow", "Da"))
        self.deletion_no_radio_button.setText(_translate("MainWindow", "Nu"))
        self.label.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">DETALII RESTRUCTURARE</span></p></body></html>"))
        self.label_4.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">Pentru numele regionalei, completati fara DRPD</span></p></body></html>"))
        self.label_5.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">Atentie! Stergerea folderului initial este ireversibila.</span></p></body></html>"))
        self.help_button.setText(_translate("MainWindow", "  AJUTOR  "))
        self.exit_button.setText(_translate("MainWindow", "IESIRE"))
        self.db_button.setText(_translate("MainWindow", "Accesare Baza de Date"))
        self.selected_folders_label.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" color:#6b7179;\">Niciun folder selectat</span></p></body></html>"))
        self.folder_selection_button.setText(_translate("MainWindow", "Selecteaza un folder nou"))
        self.folder_selection_button_2.setText(_translate("MainWindow", "Reseteaza folderele selectate"))
        self.label_6.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">SELECTARE FOLDERE</span></p></body></html>"))
        self.label_8.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">Folderele adaugate trebuie sa fie </span></p><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">cele dinaintea folderului ISAF.</span></p></body></html>"))
        self.label_9.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">Se pot restructura mai multe foldere </span></p><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">deodata, dar trebuie adaugate pe rand.</span></p></body></html>"))
        self.restructure_button.setText(_translate("MainWindow", "RESTRUCTUREAZA"))

    def open_help(self):
        webbrowser.open('https://gitlab.com/tudorBatica/file-tree')

    def close_window(self):
        MainWindow.close()

    def select_new_folder(self):
        folder = str(QFileDialog.getExistingDirectory(None, "Select Directory"))
        if folder:
            self.selected_folders.append(folder)
        folders = ""
        for dir in self.selected_folders:
            folders += dir + '\n'
        self.selected_folders_label.setText(folders)

    def reset_selected_folders(self):
        self.selected_folders = []
        self.selected_folders_label.setText("Niciun folder selectat")

    def verify_user_input(self):
        # check regional name
        regional_name = self.regional_text_edit.toPlainText()
        if not regional_name:
            return "Introduceti numele regionalei."
        # check deletion option
        if not self.deletion_yes_radio_button.isChecked() and not self.deletion_no_radio_button.isChecked():
            return "Selectati o optiune pentru stergerea folderului initial."
        # check selected folders
        if not self.selected_folders:
            return "Selectati un folder pentru restructurare."
        # all good
        return ""

    def restructure(self):
        user_input_verification = self.verify_user_input()
        if user_input_verification:
            message_box = QMessageBox.warning(None, "Completare Date", user_input_verification, QMessageBox.Ok)
            return
        
        regional_name = self.regional_text_edit.toPlainText()
        trimester = str(self.trimester_spin_box.value())
        delete_initial_folder = True
        if self.deletion_no_radio_button.isChecked():
            delete_initial_folder = False
        results = restructurer.restructure_multiple_folder_trees(self.selected_folders, regional_name, trimester, delete_initial_folder)
        self.open_report_window(results)
    
    def open_report_window(self, results):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_ReportWindow()
        self.ui.setupUi(self.window, self.selected_folders, results)
        self.window.show()
    
    def open_database_window(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_DBWindow()
        self.ui.setupUi(self.window)
        self.window.show()
    
app = QtWidgets.QApplication(sys.argv)
MainWindow = QtWidgets.QMainWindow()
ui = Ui_MainWindow()
ui.setupUi(MainWindow)
MainWindow.show()
sys.exit(app.exec_()) 
