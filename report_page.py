from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox

class Ui_ReportWindow(object):
    def setupUi(self, ReportWindow, folders, report_results):
        ReportWindow.setObjectName("ReportWindow")
        ReportWindow.resize(1272, 878)
        ReportWindow.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(240, 241, 243, 255), stop:1 rgba(230, 233, 240, 255));")
        self.centralwidget = QtWidgets.QWidget(ReportWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 20, 381, 231))
        self.frame.setStyleSheet("background-color: #fafafa;")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.label = QtWidgets.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(10, 10, 361, 51))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_7 = QtWidgets.QLabel(self.frame)
        self.label_7.setGeometry(QtCore.QRect(100, 80, 171, 91))
        self.label_7.setText("")
        self.label_7.setPixmap(QtGui.QPixmap("success_icon.png"))
        self.label_7.setScaledContents(True)
        self.label_7.setObjectName("label_7")
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setGeometry(QtCore.QRect(10, 270, 381, 231))
        self.frame_2.setStyleSheet("background-color: #fafafa;")
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.label_3 = QtWidgets.QLabel(self.frame_2)
        self.label_3.setGeometry(QtCore.QRect(10, 10, 361, 51))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.label_8 = QtWidgets.QLabel(self.frame_2)
        self.label_8.setGeometry(QtCore.QRect(140, 70, 91, 91))
        self.label_8.setText("")
        self.label_8.setPixmap(QtGui.QPixmap("partial_icon.png"))
        self.label_8.setScaledContents(True)
        self.label_8.setObjectName("label_8")
        self.label_5 = QtWidgets.QLabel(self.frame_2)
        self.label_5.setGeometry(QtCore.QRect(0, 170, 361, 51))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.frame_3 = QtWidgets.QFrame(self.centralwidget)
        self.frame_3.setGeometry(QtCore.QRect(10, 520, 381, 231))
        self.frame_3.setStyleSheet("background-color: #fafafa;")
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.label_4 = QtWidgets.QLabel(self.frame_3)
        self.label_4.setGeometry(QtCore.QRect(10, 10, 361, 51))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift SemiBold")
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.label_9 = QtWidgets.QLabel(self.frame_3)
        self.label_9.setGeometry(QtCore.QRect(140, 90, 91, 91))
        self.label_9.setText("")
        self.label_9.setPixmap(QtGui.QPixmap("failed_icon.png"))
        self.label_9.setScaledContents(True)
        self.label_9.setObjectName("label_9")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(380, 800, 511, 31))
        self.pushButton.setStyleSheet("color: white;\n"
"font: 63 8pt \"Bahnschrift SemiBold\";\n"
"background-color: #3597f9;\n"
"font-weight: bold;\n"
"font-size: 16px;\n"
"border-radius: 5px;\n"
"\n"
"\n"
"")
        self.pushButton.setObjectName("pushButton")
        self.selected_folders_scroll_area = QtWidgets.QScrollArea(self.centralwidget)
        self.selected_folders_scroll_area.setGeometry(QtCore.QRect(420, 20, 821, 231))
        self.selected_folders_scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.selected_folders_scroll_area.setWidgetResizable(True)
        self.selected_folders_scroll_area.setObjectName("selected_folders_scroll_area")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 798, 229))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.success_label = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.success_label.setFont(font)
        self.success_label.setObjectName("success_label")
        self.verticalLayout.addWidget(self.success_label)
        self.selected_folders_scroll_area.setWidget(self.scrollAreaWidgetContents)
        self.selected_folders_scroll_area_2 = QtWidgets.QScrollArea(self.centralwidget)
        self.selected_folders_scroll_area_2.setGeometry(QtCore.QRect(420, 270, 821, 231))
        self.selected_folders_scroll_area_2.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.selected_folders_scroll_area_2.setWidgetResizable(True)
        self.selected_folders_scroll_area_2.setObjectName("selected_folders_scroll_area_2")
        self.scrollAreaWidgetContents_2 = QtWidgets.QWidget()
        self.scrollAreaWidgetContents_2.setGeometry(QtCore.QRect(0, 0, 798, 229))
        self.scrollAreaWidgetContents_2.setObjectName("scrollAreaWidgetContents_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents_2)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.partial_label = QtWidgets.QLabel(self.scrollAreaWidgetContents_2)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.partial_label.setFont(font)
        self.partial_label.setObjectName("partial_label")
        self.verticalLayout_2.addWidget(self.partial_label)
        self.selected_folders_scroll_area_2.setWidget(self.scrollAreaWidgetContents_2)
        self.selected_folders_scroll_area_3 = QtWidgets.QScrollArea(self.centralwidget)
        self.selected_folders_scroll_area_3.setGeometry(QtCore.QRect(420, 530, 821, 231))
        self.selected_folders_scroll_area_3.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.selected_folders_scroll_area_3.setWidgetResizable(True)
        self.selected_folders_scroll_area_3.setObjectName("selected_folders_scroll_area_3")
        self.scrollAreaWidgetContents_3 = QtWidgets.QWidget()
        self.scrollAreaWidgetContents_3.setGeometry(QtCore.QRect(0, 0, 798, 229))
        self.scrollAreaWidgetContents_3.setObjectName("scrollAreaWidgetContents_3")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents_3)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.failed_label = QtWidgets.QLabel(self.scrollAreaWidgetContents_3)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.failed_label.setFont(font)
        self.failed_label.setObjectName("failed_label")
        self.verticalLayout_3.addWidget(self.failed_label)
        self.selected_folders_scroll_area_3.setWidget(self.scrollAreaWidgetContents_3)
        ReportWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(ReportWindow)
        self.statusbar.setObjectName("statusbar")
        ReportWindow.setStatusBar(self.statusbar)

        self.report_results = report_results
        self.folders = folders

        self.retranslateUi(ReportWindow)
        QtCore.QMetaObject.connectSlotsByName(ReportWindow)

        self.interpret_and_show_report_results()
        self.pushButton.clicked.connect(lambda : ReportWindow.close())

        

    def retranslateUi(self, ReportWindow):
        _translate = QtCore.QCoreApplication.translate
        ReportWindow.setWindowTitle(_translate("ReportWindow", "MainWindow"))
        self.label.setText(_translate("ReportWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">RESTRUCTURARI CU SUCCES</span></p></body></html>"))
        self.label_3.setText(_translate("ReportWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">RESTRUCTURARI PARTIALE</span></p></body></html>"))
        self.label_5.setText(_translate("ReportWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">Cateva posturi nu au putut fi restructurate.</span></p></body></html>"))
        self.label_4.setText(_translate("ReportWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#6b7179;\">RESTRUCTURARI ESUATE</span></p></body></html>"))
        self.pushButton.setText(_translate("ReportWindow", "Inchide Raportul"))
        self.success_label.setText(_translate("ReportWindow", "<html><head/><body><p align=\"center\"><span style=\" color:#6b7179;\">Niciun folder</span></p></body></html>"))
        self.partial_label.setText(_translate("ReportWindow", "<html><head/><body><p align=\"center\"><span style=\" color:#6b7179;\">Niciun folder </span></p></body></html>"))
        self.failed_label.setText(_translate("ReportWindow", "<html><head/><body><p align=\"center\"><span style=\" color:#6b7179;\">Niciun folder </span></p></body></html>"))

    def interpret_and_show_report_results(self):
        successful = ""
        partial = ""
        failed = ""
        for (folder, result) in zip(self.folders, self.report_results):
            if result[0] is True:
                successful += folder + '\n'
            
            elif result[0] is False and not len(result[1]):
                failed += folder + '\n'
                
            else:
                partial += "Pentru folderul\n" + folder + "\n Nu au fost restructurate posturile: "
                for post in result[1]:
                    partial += post + ", "
                partial += '\n\n'
        
        if not successful:
            successful = "Niciun folder"
        if not partial:
            partial = "Niciun folder"
        if not failed:
            failed = "Niciun folder"

        self.success_label.setText(successful)
        self.partial_label.setText(partial)
        self.failed_label.setText(failed)