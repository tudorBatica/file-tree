import os
from database_manager import DBManager

def check_folder_tree_structure(folder_path):
    """ 
    Checks whether the folder tree has the correct structure.
    
    Parameters:
    ---
    - folder_path: (string)\n
    The absolute path of the root folder.

    Return:
    ---
    (tuple(bool, list of strings))\n
    The bool represents if the folder tree has the correct structure.
    The list of strings is a list of post folders which have an incorrect
    structure.
    """
    
    if not is_first_level_of_folder_tree_correctly_structured(folder_path):
        return (False, [])
    
    posts_parent_folder_path = os.path.join(folder_path, "ISAF")
    incorrect_post_folders = get_incorrect_post_folders(posts_parent_folder_path)
    if len(incorrect_post_folders):
        return (False, incorrect_post_folders)

    return (True, [])


def is_first_level_of_folder_tree_correctly_structured(path):
    """
    Checks if there is only one directory, called 'ISAF'
    on the first level of the tree
    
    Parameters:
    ---
    - path(string):\n
    The path of the first level.

    Return:
    ---
    boolean. 

    """
    dirs = os.listdir(path)
    if len(dirs) != 1:
        return False

    if dirs[0] != "ISAF":
        return False
    
    return True

def get_incorrect_post_folders(parent_folder_path):
    """
    Returns a list of the incorrectly structured post folders.

    Parameters:
    ---
    - parent_folder_path(string):
    The path of the folder that contains the post folders.
    """

    post_folders = os.listdir(parent_folder_path)
    incorrect_posts = []
    
    for folder in post_folders:
        folder_path = os.path.join(parent_folder_path, folder)
        
        if not is_post_in_database(folder):
            incorrect_posts.append(os.path.basename(os.path.normpath(folder_path)))
        
        if not is_post_folder_correctly_structured(folder_path):
            incorrect_posts.append(os.path.basename(os.path.normpath(folder_path)))

    return incorrect_posts

def is_post_folder_correctly_structured(folder_path):
    """
    Checks if a post folder is correctly structured.
    Parameters:
    ---
    - folder_path(string)

    Returns:
    ---
    boolean.
    """

    # there should be one and only one child directory
    dirs = os.listdir(folder_path)
    if len(dirs) != 1:
        return False
    
    # in the child directory, there should be 3 folders
    dirs = os.listdir(os.path.join(folder_path, dirs[0]))
    if len(dirs) != 3:
        return False

    return True

def is_post_in_database(post):
    """
    Checks whether or not a given post is in the relevant 
    module folders database.
    Parameters:
    ---
    - post

    Returns:
    ---
    boolean.
    """
    post_relevant_module_folder = DBManager().fetch_post_module(int(post))
    if post_relevant_module_folder is None:
        return False
    return True