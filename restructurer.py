import folder_tree_structure_checker as checker
import os
import shutil
from database_manager import DBManager
from distutils.dir_util import copy_tree

def restructure_multiple_folder_trees(folder_trees, regional_name, trimester_index, delete_original):
    """
    Restructures a list of folder trees.
    Parameters
    ---
    - folder_trees (list of strings)
    The list of absolute paths of the root folders.

    - regional_name: (string)

    - trimester_index: (string)

    - delete_original: (bool)
    Whether to delete the original root folders or not 

    Return
    ---
    A list that contains a tuple of type (bool, list of strings) for each 
    folder tree, where the boolean represents if the restructuring 
    has been successful or not and the list of strings represents the 
    posts files that have not been restructured.
    """
    results = []
    for folder_tree in folder_trees:
        # check the folder tree
        checker_results = checker.check_folder_tree_structure(folder_tree)
        
        if checker_results == (False, []):
            # the folder tree cannot be restructured
            results.append((False, []))
            continue
        
        if checker_results[0] == False and checker_results[1] != []:
            # some post folders cannot be restructured
            is_restructuring_successful = restructure_folder_tree(folder_tree, regional_name, trimester_index, checker_results[1], False)
            if is_restructuring_successful:
                results.append((False, checker_results[1]))
            else:
                results.append((False, []))
        
        if checker_results[0]:
            # the tree is all good
            is_restructuring_successful = restructure_folder_tree(folder_tree, regional_name, trimester_index, [], delete_original)
            if is_restructuring_successful:
                results.append((True, []))
            else:
                results.append(False, [])
        
    return results


def restructure_folder_tree(folder_path, regional_name, trimester_index, post_folders_to_exclude, delete_original):
    """ 
    Restructures the folder tree. 
    
    Parameters
    ---
    - folder_path: (string)
    The absolute path of the root folder.

    - regional_name: (string)

    - trimester_index: (string)

    - post_folders_to_exclude: (list of strings)
    A list of the post folders that should be ignored.

    - delete_original: (bool)
    Whether to delete the original folder or not

    Return 
    ---
    Returns whether successful or not.
    """

    # create the new root folder
    new_root_folder_path = os.path.join(os.path.dirname(folder_path), "DRDP" + regional_name)
    new_root_folder_path = create_new_folder(new_root_folder_path)
    if not new_root_folder_path:
        return False
    
    # create the new trimester folder
    trimester_folder_path = os.path.join(new_root_folder_path, "TRIM" + trimester_index)
    if not create_new_folder(trimester_folder_path):
        return False

    # copy and restructure the post folders
    original_posts_parent_folder = os.path.join(folder_path, "ISAF")
    for folder in os.listdir(original_posts_parent_folder):
        if folder in post_folders_to_exclude:
            continue
        source_post_folder = os.path.join(original_posts_parent_folder, folder)
        destination_post_folder = os.path.join(trimester_folder_path, folder)
        if not copy_and_modify_post_folder(source_post_folder, destination_post_folder):
            return False
    
    if delete_original:
        shutil.rmtree(folder_path, ignore_errors = True)

    return True


def create_new_folder(folder_path):
    """ 
    Tries to create the folder at the given path.
    If the folder already exists, it adds a counter to the folder path

    Parameters
    ---
    folder_path(string).\n

    Return
    ---
    (string)\n
    Returns the path of the created folder, or "" if unsuccessful
    """
    try:
        # if folder already exists, add a counter
        if os.path.isdir(folder_path):
            counter = 1
            folder_path = os.path.normpath(folder_path)
            new_folder_path = folder_path
            while os.path.isdir(new_folder_path):
                new_folder_path = folder_path + str(counter)
                counter += 1
            folder_path = new_folder_path
        os.mkdir(folder_path)
    except Exception as e:
        print(str(e))
        return ""
    
    return folder_path


def copy_and_modify_post_folder(source_path, destination_path):
    """ 
    Copies a post folder, removing the first child directory
    of the post folder.
    
    Parameters:
    ---
    source_path(string):\n
    The original post folder.

    destionation_path(string):\n
    Where to create the new folder.

    Return:
    ---
    boolean.\n
    Whether the operation was successful or not.
    """

    # create the new directory
    if not create_new_folder(destination_path):
        return False
    
    # copy the relevant module folder
    parent_directory_of_module_folders = os.path.join(source_path, os.listdir(source_path)[0])
    relevant_module_folder = get_relevant_module_folder_for_post(os.path.basename(source_path))
    
    if not relevant_module_folder:
        return False
    
    copy_tree(os.path.join(parent_directory_of_module_folders, relevant_module_folder), 
              os.path.join(destination_path, relevant_module_folder))
    
    return True


def get_relevant_module_folder_for_post(post_number):
    """
    Returns the module folder that is relevant 
    for a certain post
    Parameter:
    ---
    post_number: int

    Returns:
    ---
    The module folder name or an empty string 
    if something went wrong.
    """
    relevant_module_folder = DBManager().fetch_post_module(int(post_number))
    if relevant_module_folder is None:
        return ""
    
    return f'Modul{relevant_module_folder}'