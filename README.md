# Restructurare fisiere ISAF

O aplicatie Python pentru restructurarea fisierelor ISAF.

## Cum se instaleaza

Aplicatia este disponibila doar pentru Windows.  
Pentru a instala aplicatia pe PC-ul dvs., descarcati acest proiect.  
Intrati in output/RestructurareISAF. Aici veti gasi arhiva Restructurare_ISAF_v1.1.  
  
Alternativ, puteti construi aplicatia folosind codul sursa si Python3. 
Pentru aceasta veti avea nevoie si de PyQt5 si MySQL Connector/Python. 
Pentru a instala PyQt5 folosind pip, folositi comanda `pip install PyQt5`.  
Pentru a instala MySQL Connector folosind pip, folositi comanda `pip install mysql-connector-python`.  
  

## Cum se foloseste

### Completarea Datelor

#### Date Restructurare
Din prima fereastra a aplicatiei, puteti completa informatiile referitoare la restructurare.  
<img src="documentation_img/restructurare.png"  width="868" height="600">  
Cand selectati folderele, trebuie selectate cele dinaintea folderelor ISAF.

#### Selectarea Modulului Relevant
Pentru a selecta modulul relevant pentru fiecare post, apasati butonul din stanga sus intitulat Accesare Baza de Date.
O noua fereastra se va deschide, de unde veti putea modifica pentru fiecare post modulul relevant sau puteti adauga un 
post nou.  
<img src="documentation_img/dbview.png"  width="600" height="600">  
Completati campul 'Numarul Postului', apoi selectati Modulul.  
Daca postul exista deja in baza de date, modulul acestuia va fi modificat cu modulul selectat de dvs.  
Daca postul nu exista, se va crea o noua inregistrare in baza de date.  

### Raportul de Restructurare  
Dupa ce ati completat toate datele, apasati butonul de Restructurare. 
O noua fereastra va aparea, unde veti putea observa un raport al restructurarii.    
<img src="documentation_img/Report.png"  width="868" height="600">  
In partea de sus, vor aparea folderele care au fost restructurate cu succes.  
In partea de mijloc, vor aparea folderele care au fost restructurate doar partial(i.e.: care au avut unul sau mai multe foldere de post care nu respecta structura standard). Folderele post care nu respecta structura standard NU vor fi mutate in noul folder.  
In partea de jos, vor aparea folderele pentru care restructurarea a esuat.  
Observatie: Pentru folderele restructurate partial sau pentru care operatia a esuat, NU va fi sters folderul initial, chiar daca optiunea de stergere a fost bifata. 


